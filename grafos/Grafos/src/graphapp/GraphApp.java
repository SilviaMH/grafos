/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package graphapp;

import graphlib.Graph;
import graphlib.GraphGenerator;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import graphlib.Vertex;

/**
 * Tests the graph library
 *
 * @author Sil
 */
public class GraphApp {

    /**
     * Tests graph methods
     * @param args the command line arguments
     * @throws java.lang.Exception
     */
    public static void main(String[] args) throws Exception {
        try {
            //GraphGeneratorTest();
            //BfsDfsTest();
            //Dijkstra();
            MstTest();
        } catch (Exception ex) {
            System.out.print(ex.getMessage());
        }
    }

    /**
     * Tests Kruskal and Prim algorithms
     *
     * @throws Exception
     */
    public static void MstTest() throws Exception {
        try {
            Graph gil10 = GraphGenerator.CreateGraphGilbert(10, 0.45, false);
            Graph wgGil10 = gil10.EdgeValues(1, 100);
            wgGil10.PrintToFile(false, true, false, "Gilbert10");
            Graph kdGil10 = wgGil10.Kruskal_D();
            kdGil10.PrintToFile(false, true, true, "Gilbert10_MST_KD");
            Graph kiGil10 = wgGil10.Kruskal_I();
            kiGil10.PrintToFile(false, true, true, "Gilbert10_MST_KI");
            Graph primGil10 = wgGil10.Prim();
            primGil10.PrintToFile(false, true, true, "Gilbert10_MST_Prim");

            Graph gil150 = GraphGenerator.CreateGraphGilbert(150, 0.45, false);
            Graph wgGil150 = gil150.EdgeValues(1, 100);
            wgGil150.PrintToFile(false, false, false, "Gilbert150");
            Graph kdGil150 = wgGil150.Kruskal_D();
            kdGil150.PrintToFile(false, true, true, "Gilbert150_MST_KD");
            Graph kiGil150 = wgGil150.Kruskal_I();
            kiGil150.PrintToFile(false, true, true, "Gilbert150_MST_KI");
            Graph primGil150 = wgGil150.Prim();
            primGil150.PrintToFile(false, true, true, "Gilbert150_MST_Prim");

            Graph erdos10 = GraphGenerator.CreateErdosRenyi(10, 15, false);
            Graph wgErdos10 = erdos10.EdgeValues(1, 100);
            wgErdos10.PrintToFile(false, true, false, "Erdos10");
            Graph kdErdos10 = wgErdos10.Kruskal_D();
            kdErdos10.PrintToFile(false, true, true, "Erdos10_MST_KD");
            Graph kiErdos10 = wgErdos10.Kruskal_I();
            kiErdos10.PrintToFile(false, true, true, "Erdos10_MST_KI");
            Graph primErdos10 = wgErdos10.Prim();
            primErdos10.PrintToFile(false, true, true, "Erdos10_MST_Prim");

            Graph erdos150 = GraphGenerator.CreateErdosRenyi(150, 500, false);
            Graph wgErdos150 = erdos150.EdgeValues(1, 100);
            wgErdos150.PrintToFile(false, true, false, "Erdos150");
            Graph kdErdos150 = wgErdos150.Kruskal_D();
            kdErdos150.PrintToFile(false, true, true, "Erdos150_MST_KD");
            Graph kiErdos150 = wgErdos150.Kruskal_I();
            kiErdos150.PrintToFile(false, true, true, "Erdos150_MST_KI");
            Graph primErdos150 = wgErdos150.Prim();
            primErdos150.PrintToFile(false, true, true, "Erdos150_MST_Prim");

            Graph barabasi10 = GraphGenerator.CreateBarabasiAlbertGraph(10, 5, false);
            Graph wgBarabasi10 = barabasi10.EdgeValues(1, 100);
            wgBarabasi10.PrintToFile(false, true, false, "Barabasi10");
            Graph kdBarabasi10 = wgBarabasi10.Kruskal_D();
            kdBarabasi10.PrintToFile(false, true, true, "Barabasi10_MST_KD");
            Graph kiBarabasi10 = wgBarabasi10.Kruskal_I();
            kiBarabasi10.PrintToFile(false, true, true, "Barabasi10_MST_KI");
            Graph primBarabasi10 = wgBarabasi10.Prim();
            primBarabasi10.PrintToFile(false, true, true, "Barabasi10_MST_Prim");

            Graph barabasi150 = GraphGenerator.CreateBarabasiAlbertGraph(150, 4, false);
            Graph wgBarabasi150 = barabasi150.EdgeValues(1, 100);
            wgBarabasi150.PrintToFile(false, true, false, "Barabasi150");
            Graph kdBarabasi150 = wgBarabasi150.Kruskal_D();
            kdBarabasi150.PrintToFile(false, true, true, "Barabasi150_MST_KD");
            Graph kiBarabasi150 = wgBarabasi150.Kruskal_I();
            kiBarabasi150.PrintToFile(false, true, true, "Barabasi150_MST_KI");
            Graph primBarabasi150 = wgBarabasi150.Prim();
            primBarabasi150.PrintToFile(false, true, true, "Barabasi150_MST_Prim");

            Graph geo10 = GraphGenerator.CreateGeographicalGraph(10, 0.5, false);
            Graph wgGeo10 = geo10.EdgeValues(1, 100);
            wgGeo10.PrintToFile(false, true, false, "Geo10");
            Graph kdGeo10 = wgGeo10.Kruskal_D();
            kdGeo10.PrintToFile(false, true, true, "Geo10_MST_KD");
            Graph kiGeo10 = wgGeo10.Kruskal_I();
            kiGeo10.PrintToFile(false, true, true, "Geo10_MST_KI");
            Graph primGeo10 = wgGeo10.Prim();
            primGeo10.PrintToFile(false, true, true, "Geo10_MST_Prim");

            Graph geo150 = GraphGenerator.CreateGeographicalGraph(150, 0.2, false);
            Graph wgGeo150 = geo150.EdgeValues(1, 100);
            wgGeo150.PrintToFile(false, true, false, "Geo150");
            Graph kdGeo150 = wgGeo150.Kruskal_D();
            kdGeo150.PrintToFile(false, true, true, "Geo150_MST_KD");
            Graph kiGeo150 = wgGeo150.Kruskal_I();
            kiGeo150.PrintToFile(false, true, true, "Geo150_MST_KI");
            Graph primGeo150 = wgGeo150.Prim();
            primGeo150.PrintToFile(false, true, true, "Geo150_MST_Prim");

        } catch (Exception ex) {
            System.out.print(ex.getMessage());
        }
    }

    /**
     * Tests Dijkstra algorithm
     *
     * @throws Exception
     */
    public static void Dijkstra() throws Exception {
        try {
            Vertex s = new Vertex(1);
            Graph gil10 = GraphGenerator.CreateGraphGilbert(10, 0.45, false);
            Graph gil150 = GraphGenerator.CreateGraphGilbert(150, 0.45, false);
            Graph wgGil10 = gil10.EdgeValues(1, 100);
            Graph wgGil150 = gil150.EdgeValues(1, 100);
            wgGil10.PrintToFile(false, true, false, "Gilbert10");
            wgGil150.PrintToFile(false, false, false, "Gilbert150");
            Graph pathGil10 = wgGil10.Dijkstra(s);
            Graph pathGil150 = wgGil150.Dijkstra(s);
            pathGil10.PrintToFile(true, true, false, "Gilbert10_DijkstraTree");
            pathGil150.PrintToFile(true, true, false, "Gilbert150_DijkstraTree");

            Graph erdos10 = GraphGenerator.CreateErdosRenyi(10, 15, false);
            Graph erdos150 = GraphGenerator.CreateErdosRenyi(150, 500, false);
            Graph wgErdos10 = erdos10.EdgeValues(1, 100);
            Graph wgErdos150 = erdos150.EdgeValues(1, 100);
            wgErdos10.PrintToFile(false, true, false, "Erdos10");
            wgErdos150.PrintToFile(false, false, false, "Erdos150");
            Graph pathErdos10 = wgErdos10.Dijkstra(s);
            Graph pathErdos150 = wgErdos150.Dijkstra(s);
            pathErdos10.PrintToFile(true, true, false, "Erdos10_Dijkstra");
            pathErdos150.PrintToFile(true, true, false, "Erdos150_Dijkstra");

            Graph barabasi10 = GraphGenerator.CreateBarabasiAlbertGraph(10, 5, false);
            Graph barabasi50 = GraphGenerator.CreateBarabasiAlbertGraph(150, 4, false);
            Graph wgBarabasi10 = barabasi10.EdgeValues(1, 100);
            Graph wgBarabasi150 = barabasi50.EdgeValues(1, 100);
            wgBarabasi10.PrintToFile(false, true, false, "Barabasi10");
            wgBarabasi150.PrintToFile(false, true, false, "Barabasi150");
            Graph pathBarabasi10 = wgBarabasi10.Dijkstra(s);
            Graph pathBarabasi150 = wgBarabasi150.Dijkstra(s);
            pathBarabasi10.PrintToFile(true, true, false, "Barabasi10_Dijkstra");
            pathBarabasi150.PrintToFile(true, true, false, "Barabasi150_Dijkstra");

            Graph geo10 = GraphGenerator.CreateGeographicalGraph(10, 0.5, false);
            Graph geo150 = GraphGenerator.CreateGeographicalGraph(150, 0.2, false);
            Graph wgGeo10 = geo10.EdgeValues(1, 100);
            Graph wgGeo150 = geo150.EdgeValues(1, 100);
            wgGeo10.PrintToFile(false, true, false, "Geo10");
            wgGeo150.PrintToFile(false, true, false, "Geo150");
            Graph pathGeo10 = wgGeo10.Dijkstra(s);
            Graph pathGeo150 = wgGeo150.Dijkstra(s);
            pathGeo10.PrintToFile(true, true, false, "Geo10_Dijkstra");
            pathGeo150.PrintToFile(true, true, false, "Geo150_Dijkstra");

        } catch (Exception ex) {
            System.out.print(ex.getMessage());
        }
    }

    /**
     * Tests BFS and DFS algorithms
     *
     * @throws Exception
     */
    public static void BfsDfsTest() throws Exception {
        try {
            Graph result = GraphGenerator.CreateGraphGilbert(20, 0.55, false);
            result.PrintToFile(false, false, false, "Graph");
            Vertex source = new Vertex(1);
            Graph bfs = result.BFS(source);
            bfs.PrintToFile(false, false, false, "BFSTree");
            Graph dfsI = result.DFS_I(source);
            dfsI.PrintToFile(false, false, false, "DFS_I_Tree");
            Graph dfsR = result.DFS_R(source);
            dfsR.PrintToFile(false, false, false, "DFS_R_Tree");
        } catch (Exception ex) {
            System.out.print(ex.getMessage());
        }
    }

    /**
     * Tests graph random generator
     * @throws Exception 
     */
    public static void GraphGeneratorTest() throws Exception {
        System.out.println("Graph builder");
        System.out.println("Choose the type of Graph builder");
        System.out.println("1: Gilbert");
        System.out.println("2: Erdos-Renyi ");
        System.out.println("3: Geographical");
        System.out.println("4: Barabasi-Albert");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String option = br.readLine();
        int optionGraphGenerator = ValidateInput(option);
        Graph result = null;

        switch (optionGraphGenerator) {
            case 1:
                System.out.println("Enter the number of vertices (e.g. 30)");
                int vertex = ValidateInput(br.readLine());
                System.out.println("Enter the number of probability between 0.0 and 1.0 (e.g. 0.3)");
                double probability = ValidateInputDouble(br.readLine());
                System.out.println("Self-loops allowed? (true/false)");
                boolean selfLoopsAllowed = ValidateInputBoolean(br.readLine());
                result = GraphGenerator.CreateGraphGilbert(vertex, probability, selfLoopsAllowed);
                break;

            case 2:
                System.out.println("Enter the number of vertices (e.g. 30)");
                int vertexRendos = ValidateInput(br.readLine());
                System.out.println("Enter the number of edges (e.g. 300)");
                int edgeRendos = ValidateInput(br.readLine());
                System.out.println("Self-loops allowed? (true/false)");
                boolean selfLoopsAllowedE = ValidateInputBoolean(br.readLine());
                result = GraphGenerator.CreateErdosRenyi(vertexRendos, edgeRendos, selfLoopsAllowedE);
                break;

            case 3:
                System.out.println("Enter the number of vertices (e.g. 30)");
                int vertexGeo = ValidateInput(br.readLine());
                System.out.println("Enter the distance(e.g. 0.5)");
                double distance = ValidateInputDouble(br.readLine());
                System.out.println("Self-loops allowed? (true/false)");
                boolean selfLoopsAllowedGeo = ValidateInputBoolean(br.readLine());
                result = GraphGenerator.CreateGeographicalGraph(vertexGeo, distance, selfLoopsAllowedGeo);
                break;

            case 4:
                System.out.println("Enter the number of vertices (e.g. 30)");
                int vertexBar = ValidateInput(br.readLine());
                System.out.println("Enter the degree (e.g. 5)");
                int degree = ValidateInput(br.readLine());
                System.out.println("Self-loops allowed? (true/false)");
                boolean selfLoopsAllowedBar = ValidateInputBoolean(br.readLine());
                result = GraphGenerator.CreateBarabasiAlbertGraph(vertexBar, degree, selfLoopsAllowedBar);
                break;
        }
        result.PrintToFile(false, false, false,"RandomGraph");
        System.out.println("Graphs created successfully.");
    }

    /**
     * Input conversion from string type to Integer
     * @param inputOption
     * @return int converted
     * @throws GraphException 
     */
    private static int ValidateInput(String inputOption) throws Exception{
        try{
            int response = Integer.parseInt(inputOption);
            return response;
        }
        catch(NumberFormatException error){
            throw new Exception("Invalid format! "+ error.getMessage());
        }
    }
    
    /**
     * Input conversion from string to double
     * @param inputOption
     * @return
     * @throws GraphException 
     */
    private static double ValidateInputDouble(String inputOption) throws Exception{
        try{
            double response = Double.parseDouble(inputOption);
            return response;
        }
        catch(NumberFormatException error){
            throw new Exception("Invalid format! "+ error.getMessage());
        }
    }
    
    /**
     * Input conversion from string to boolean
     * @param inputOption
     * @return
     * @throws GraphException 
     */
    private static boolean ValidateInputBoolean(String inputOption) throws Exception{
        try{
            boolean response = Boolean.parseBoolean(inputOption);
            return response;
        }
        catch(NumberFormatException error){
            throw new Exception("Invalid format! "+ error.getMessage());
        }
    }
}
