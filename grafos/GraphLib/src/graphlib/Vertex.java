package graphlib;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *Class that represents any sort of Node
 * @author Sil
 */
public class Vertex implements Comparable<Vertex> {
    
    /**
     * Value or identifier on the node, 
     */
    public Integer id;
    
    /**
     * Number of Edges connected to a node
     * deg(node) 
     */
    public Integer degree;
    
    /**
     * distance from a source node
     */
    public Float distance;
    
    /**
     * description of the node
     */
    public String description;
    
    /**
     * Indicates whether the vertex has been explored or not
     */
    public Boolean explored;
    
    /**
     * parent of vertex
     */
    public Vertex parent;
   
    /**
     * Position on x 
     */
    public Double X;
    
    /**
     * Position on y
     */
    public Double Y;
    
    /**
     * Position on z
     */
    public Integer z;
    
    /**
     * Builder of vertex
     */
    public Vertex(int id){
        this.id = id;
    }
    
    @Override
    public int compareTo(Vertex other) {
        if (this.distance < other.distance){
            return -1;
        } else if (this.distance > other.distance){
            return 1;
        } else {
          return 0;
        }
    }
}
