package graphlib;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.HashMap;
import java.util.Random;

/**
 * Class that creates Random Graphs
 * @author Sil
 */
public class GraphGenerator {
    
    /**
     * Creates a Random Graph with the Gilbert method
     * @param vertexNumber vertex number
     * @param probability given by user
     * @param selfLoopAllowed indicates if the graph allows self-loops
     * @return a random graph
     */
    public static Graph CreateGraphGilbert(Integer vertexNumber, Double probability, Boolean selfLoopAllowed) {
    
        if(probability< 0.0 || probability>1.0){
           throw new IllegalArgumentException("Probability must be less or equal than 1.0 and greater than 0");
        }
        
        Graph result = new Graph();
        result.description = "Gilbert";
        result.isDirected = false;
        result.selfLoopsAllowed = selfLoopAllowed;
        
        int startingPoint = selfLoopAllowed ? 0 : 1;
        for(int vertex = 1; vertex <= vertexNumber;vertex++){
            Vertex vertexAux = new Vertex(vertex);
            result.AddVertex(vertex, vertexAux);
            for(int nextVertex = vertex + startingPoint; nextVertex<=vertexNumber; nextVertex++){
                if(IsSuccess(probability)){
                    Edge edgeAux = new Edge();
                    edgeAux.sourceID = vertex;
                    edgeAux.targetID = nextVertex;
                    result.AddEdge(vertex+"-"+nextVertex, edgeAux);  
                }
            }
        }
        
        return result;
    }
        
    /**
     * Creates a Graph with the Erdös-Rényi method
     * @param vertexNumber number of vertices
     * @param edgeNumber number of edges
     * @param selfLoopAllowed indicates if the graph allows self-loops
     * @return a random Graph
     */
    public static Graph CreateErdosRenyi(Integer vertexNumber, Integer edgeNumber, Boolean selfLoopAllowed) {
        if(vertexNumber == 0 || edgeNumber == 0){
            throw new IllegalArgumentException("Number of vertices and edges must be greater than zero");
        }
        
        if((vertexNumber*vertexNumber)<(edgeNumber)){
            throw new IllegalArgumentException("Invalid number of edges.");
        }
    
        //Create the vertices
        Graph result = new Graph();
        result.description = "ErdosRenyi";
        result.isDirected = false;
        result.selfLoopsAllowed = selfLoopAllowed;
        result.vertices = new HashMap<>();
        for(int vertexCreated = 1; vertexCreated <= vertexNumber;vertexCreated++){
            Vertex item = new Vertex(vertexCreated);
            result.AddVertex(vertexCreated, item);
        }
        
        //Creates the edges
        result.edges = new HashMap();
        for(int edgeCreated = 1; edgeCreated <= edgeNumber;edgeCreated++){
            Integer source = CreateRandomInteger(1,vertexNumber);
            Integer target = CreateRandomInteger(1,vertexNumber);
            
            if(!ExistOnGraph(result, source, target)) {
                if(!selfLoopAllowed && (source.equals(target))) {
                    edgeCreated--;
                } else {
                    Edge newEdge = new Edge();
                    newEdge.sourceID = source;
                    newEdge.targetID = target;
                    result.AddEdge(source+"-"+target, newEdge);
                }
            } else {
                edgeCreated--;
            }
        }
        
        return result;
    }
    
    /**
     * Creates a simple geographical graph
     * @param vertexNumber number of vertices
     * @param distance  maximum distance allowed between edges
     * @param selfLoopAllowed indicates if the graph allows self-loops
     * @return a random graph
     */
    public static Graph CreateGeographicalGraph(Integer vertexNumber, Double distance, Boolean selfLoopAllowed) {
        Graph result = new Graph();
        result.description = "Geographical";
        result.isDirected = false;
        result.selfLoopsAllowed = selfLoopAllowed;
        result.vertices = new HashMap<>();
        
        for(int vertexCreated = 1; vertexCreated <=vertexNumber;vertexCreated++){
            Vertex item = new Vertex(vertexCreated);
            item.X = CreateRandomNumber();
            item.Y = CreateRandomNumber();
            result.AddVertex(vertexCreated, item);
        }
         
        int startingPoint = selfLoopAllowed ? 0 : 1;
        //Create the edges
        result.edges = new HashMap();
        for(int i = 1; i <= vertexNumber; i++){
            for(int j = i + startingPoint; j<= vertexNumber; j++){
                Double euclideanDistance = Math.sqrt(Math.pow(result.vertices.get(i).X - result.vertices.get(j).X,2) + Math.pow(result.vertices.get(i).Y - result.vertices.get(j).Y,2));
                if(euclideanDistance <= distance) {
                    Edge newEdge = new Edge();
                    newEdge.sourceID = i;
                    newEdge.targetID = j;
                    result.AddEdge(i+"-"+j, newEdge);
                }
            }          
        }

        
        return result;
    }
    
    /**
     * Creates a simple geographical graph
     * @param vertexNumber number of vertices
     * @param degreeV degreeV of vertex
     * @param selfLoopAllowed indicates if the graph allows self-loops
     * @return a random graph
     */
    public static Graph CreateBarabasiAlbertGraph(Integer vertexNumber, Integer degreeV, Boolean selfLoopAllowed) {
        Graph result = new Graph();
        result.description = "BarabasiAlbert";
        result.isDirected = false;
        result.selfLoopsAllowed = selfLoopAllowed;
        result.vertices = new HashMap<>();

        //Create the edges
        result.edges = new HashMap();
        double p = 0.0;
        for(int i = 1; i <= vertexNumber; i++){
            Vertex item = new Vertex(i);
            item.degree = 0;
            result.AddVertex(i, item);
            for(int j = 1; j<= result.vertices.size(); j++){
                if(result.vertices.get(i).degree<=degreeV){
                    if((i==j)&&!selfLoopAllowed) {
                        continue;
                    } else {
                        p = 1 - (result.vertices.get(j).degree/Double.valueOf(degreeV));
                        if (CreateRandomNumber() < p) {
                            Edge newEdge = new Edge();
                            newEdge.sourceID = i;
                            newEdge.targetID = j;
                            result.AddEdge(i+"-"+j, newEdge);
                            result.vertices.get(i).degree += 1;
                            if(i!=j){
                                result.vertices.get(j).degree += 1;
                            }
                        }
                    }
                } else {
                    break;
                }
            }          
        } 
        
        return result;
    }
     
    /**
     * Validates if an edge exists
     * @param graph
     * @param source
     * @param target
     * @return 
     */
    public static Boolean ExistOnGraph(Graph graph, Integer source, Integer target){       
        Boolean exist = graph.edges.containsKey(source+"-"+target);
        Boolean existBackwards = graph.edges.containsKey(target+"-"+source);
            
        return (exist || existBackwards);
    }
    
    /**
     * Returns a random real number uniformly in [0, 1].
     * @return 
     */
     private static double CreateRandomNumber() {
        Random random = new Random();
        return random.nextDouble();
    }
    
     /** 
      * Builds a random number between minNumber and maxNumer
      * @param minNumber Lower limit
      * @param maxNumber Upper limit 
      * @return Random integer number
      */
     private static int CreateRandomInteger(Integer minNumber, Integer maxNumber){
         Random random =  new Random();
         return random.nextInt((maxNumber - minNumber) + 1) + minNumber;
     }   
     
     /**
      * Creates random number and check if it is  successful.
      * Apply the distribution of Bernoulli 
      * @param p probability
      * @return Success if it's true
      */
     private static boolean IsSuccess(double p) {
        if (!(p >= 0.0 && p <= 1.0))
            throw new IllegalArgumentException("invalid probability 2");
        return CreateRandomNumber() < p;
    }
    
}
