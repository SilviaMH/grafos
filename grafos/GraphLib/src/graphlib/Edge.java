package graphlib;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 * Class that represents any sort of Edge 
 * @author Sil
 */
public class Edge implements Comparable<Edge> { 
    
    /**
     * Identifier of the edge
     */
    public Integer Id;
    
    /**
     * Edge source Id 
     */
    public Integer sourceID;
    
    /**
     * Edge destination Id
     */
    public Integer targetID ;
    
    /**
     * weight on the edge
     */
    public Float weight;
    
    @Override
    public int compareTo(Edge other) {  
        return this.weight > other.weight ? 1 : this.weight < other.weight? -1 : 0;  
    }
}
