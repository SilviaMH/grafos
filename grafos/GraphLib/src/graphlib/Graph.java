package graphlib;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.HashSet;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Queue;
import java.util.LinkedList;
import java.util.Random;
import java.util.Stack;
import java.util.Map;
import java.util.Objects;
import java.util.PriorityQueue;

/**
 *Class that represent any sort of Graph
 * @author Sil
 */
public class Graph {
    
    /**
     * Graph's name or brief descripction
     */
    public String description;
    
     /**
     * Total weight of the MST
     */
    private Double totalWeight;
    
    /**
     * Number of total vertices
     */
    public Integer vertexCount;
    
    /**
     * Number of total edges
     */
    public Integer edgesCount;
    
    /**
     * Depth First Search Tree
     */
    private Graph dfsTree;
    
    /**
     * List of edges
     */
    public HashMap<String, Edge> edges;
    
    /**
     * List of vertices
     */
    public HashMap<Integer, Vertex> vertices;
    
    /**
     * Adjacency List that contains the relationships between vertices
     */
    public HashMap<Integer, ArrayList<Integer>> adjacencyList;
    
    /**
     * Indicates if the graph is directed
     */
    public Boolean isDirected;
    
    /**
     * Indicates if the graph allows an edge that connects a vertex to itself.
     */
    public Boolean selfLoopsAllowed;
      
    /**
     * Builder of Graph
     */
    public Graph(){
        this.vertices = new HashMap<>();
        this.edges = new HashMap();
        this.adjacencyList = new HashMap<>();
    }
    
    /**
     * Adds a new vertex
     * @param id
     * @param vertex
     */
    public void AddVertex(Integer id, Vertex vertex ){
        this.vertices.put(id, vertex);
    }
    
    /**
     * Adds a new Edge on graph and updates the adjacency list.
     * @param id
     * @param edge 
     */
    public void AddEdge(String id, Edge edge){
        this.edges.put(id, edge);
        AddVertexAdjList(edge.sourceID);
        AddVertexAdjList(edge.targetID);
        
        if (!(this.adjacencyList.containsKey(edge.sourceID) && this.adjacencyList.get(edge.sourceID).contains(edge.targetID))) { 
            this.adjacencyList.get(edge.sourceID).add(edge.targetID);
        }  
        if (!(this.adjacencyList.containsKey(edge.targetID) && this.adjacencyList.get(edge.targetID).contains(edge.sourceID))) { 
            this.adjacencyList.get(edge.targetID).add(edge.sourceID);
        }
    }
    
    /**
     * Adds a vertex to adjacency list
     * @param id Vertex id
     */
    private void AddVertexAdjList(Integer id) {
        if (!this.adjacencyList.containsKey(id)) {
            this.adjacencyList.put(id, new ArrayList<>());
        }
    }
    
    /**
     * Breath First Search algorithm
     * @param sourceVertex Root vertex
     * @return BFS Tree
     */
    public Graph BFS(Vertex sourceVertex) {
        if (!this.vertices.containsKey(sourceVertex.id)) {
            throw new IllegalArgumentException("The vertex is not contained in this graph.");
        } 
        
        Vertex s = this.vertices.get(sourceVertex.id);
        Queue<Vertex> q = new LinkedList();
        this.vertices.forEach((Integer key,Vertex value) -> {
            value.explored = false;
        });
        
        s.explored = true;
        q.add(sourceVertex);
        Graph bfsTree = new Graph();
        bfsTree.vertices = this.vertices;  
        bfsTree.description = "BFSTree";
        bfsTree.isDirected = false;
        
        while(!q.isEmpty()){
            Vertex u = q.poll();

            for(Integer adj : this.adjacencyList.get(u.id)) {   
                Vertex v = this.vertices.get(adj);
                if(!v.explored) {
                    v.explored = true;
                    Edge auxEdge =  GetEdge(u.id,v.id);
                    bfsTree.AddEdge(u.id+"-"+v.id, auxEdge);
                    q.add(v);
                }
            }
        }        
        return bfsTree;
    }
    
    /** 
     * Initializes a DFS Tree and invokes the recursive DFS method
     * @param sourceVertex Root vertex
     * @return DBS Tree
     */
    public Graph DFS_R(Vertex sourceVertex) {
        if (!this.vertices.containsKey(sourceVertex.id)) {
            throw new IllegalArgumentException("The vertex is not contained in this graph.");
        } 
        
        Vertex source = this.vertices.get(sourceVertex.id);   
        this.dfsTree = new Graph();
        this.dfsTree.vertices = this.vertices;  
        this.dfsTree.description = "DFS_R_Tree";
        this.dfsTree.isDirected = false;
        
        this.vertices.forEach((Integer key,Vertex value) -> {
            value.parent = null;
            value.explored = false;
        });
             
        DFS_Recursive(source);
        
        return dfsTree;
    }
    
    /** 
     * Recursive Depth First Search Algorithm
     * @param u Root vertex
     */
    private void DFS_Recursive(Vertex u) {
         u.explored = true;
         for(int adjv:this.adjacencyList.get(u.id)) {
             Vertex v = this.vertices.get(adjv);
             if (!v.explored) {  
                 v.parent = u;    
                 Edge auxEdge =  GetEdge(u.id,v.id);
                 this.dfsTree.AddEdge(u.id+"-"+v.id, auxEdge); 
                 DFS_Recursive(v);         
             }
         }
    }
    
    /** 
     * Iterative Depth First Search Algorithm
     * @param sourceVertex Root vertex
     * @return DBS Tree
     */
    public Graph DFS_I(Vertex sourceVertex) {
        if (!this.vertices.containsKey(sourceVertex.id)) {
            throw new IllegalArgumentException("The vertex is not contained in this graph.");
        }
        
        Vertex source = this.vertices.get(sourceVertex.id);   
        
        Stack<Vertex> s = new Stack<>();
        s.add(source);
        Graph dfsTree = new Graph();
        dfsTree.vertices = this.vertices;  
        dfsTree.description = "DFS_I_Tree";
        dfsTree.isDirected = false;
        
        this.vertices.forEach((Integer key,Vertex value) -> {
            value.parent = null;
            value.explored = false;
        });
        
        while(!s.isEmpty()) {
            Vertex u = s.peek();
            if (!u.explored) {
                u.explored = true;
                if(!Objects.equals(u.id, source.id)) {
                    Edge auxEdge = GetEdge(u.id,u.parent.id);
                    dfsTree.AddEdge(u.id+"-"+u.parent.id, auxEdge);
                }

                for(Integer adj:this.adjacencyList.get(u.id)) {   
                    Vertex v = this.vertices.get(adj);
                    s.push(v);
                    v.parent = u;
                }
            } else {
                s.pop();
            }
        }
        
        return dfsTree;
    }
    
    /**
     * Asigns random weights within a range to graph edges
     * @param min Lower limit
     * @param max Upper limit
     * @return a Graph with weights
     */
    public Graph EdgeValues(float min, float max) { 
        Random random =  new Random(); 
        
        this.edges.forEach((String key, Edge value) -> {
            value.weight = (random.nextFloat() * (max-min)) + min;
        });
        
        return this;
    }
    
    /**
     * Finds the shortest path from a source vertex to the rest of the vertices.
     * @param source A source vertex.
     * @return Tree of the shortest path.
     */
    public Graph Dijkstra(Vertex source) {       
        PriorityQueue<Vertex> pqueue = new PriorityQueue<>();
        Graph path = new Graph();
        path.vertices = this.vertices;
        path.isDirected = true;
        path.edges = new HashMap<>();
        path.description = "ShortestPathFromNode" + source.id;
        
        this.vertices.forEach((Integer key, Vertex value) -> {
            value.distance = Float.MAX_VALUE;
            value.explored = false;
            value.parent = null;
        });

        this.vertices.get(source.id).distance = 0.0f;      
        pqueue.add(this.vertices.get(source.id));
        
        while(!pqueue.isEmpty()) {
            Vertex u = pqueue.remove();
            u.explored = true;
            
            if(u.parent != null){
                Edge pathEdge = new Edge();
                pathEdge.sourceID = u.parent.id;
                pathEdge.targetID = u.id;
                pathEdge.weight = GetEdge(pathEdge.sourceID,pathEdge.targetID).weight;
                path.AddEdge(pathEdge.sourceID+"-"+pathEdge.targetID, pathEdge);
            }
                 
            for (Integer value : this.adjacencyList.get(u.id)){
                Vertex v = this.vertices.get(value);
                if(!v.explored && (v.distance > u.distance + GetEdge(u.id,value).weight)){  
                    v.distance = u.distance + GetEdge(u.id,value).weight;
                    v.parent = u;
                    if (pqueue.contains(v)){
                        pqueue.remove(v);
                    }
                    pqueue.add(v);
                }
            }
        }
        
        return path;
    }
    
     /**
     * Verifies whether a graph is connected
     * @return 
     */
    private boolean IsConnected(){
        DFS_R(new Vertex(1));
        
        for(Vertex v: this.vertices.values()){
            if(!v.explored){
                return false;
            }
        }
        
        return true;
    }
    
    /**
     * Finds Minimum Spanning Tree and calculates total weight
     * @return Minimum Spanning Tree
     */
    public Graph Kruskal_D(){
        PriorityQueue<Edge> pqueue = new PriorityQueue<>();
        Graph MST = new Graph();
        MST.description =  "KruskalD_MST";
        MST.vertices = this.vertices;
        MST.isDirected = false; 
        MST.totalWeight=0.0;
        UnionFind UF = new UnionFind(new HashSet<>(this.vertices.values())); 
        
        this.edges.forEach((String key,Edge value) -> {
            pqueue.add(value);
        });
        
        for (int i=0; i<this.edges.size(); i++) {
            Edge e = pqueue.remove();
            if(!UF.InSameSet(this.vertices.get(e.sourceID), this.vertices.get(e.targetID))){
                MST.AddEdge(e.sourceID+"-"+e.targetID, e);
                MST.totalWeight += e.weight;
                UF.Union(this.vertices.get(e.sourceID), this.vertices.get(e.targetID));
            }
        }
        
        System.out.println("Peso del MST generado por Kruskal directo: " + MST.totalWeight);
        return MST;
    }
    
    /**
     * Finds Minimum Spanning Tree and calculates total weight
     * @return Minimum Spanning Tree
     */
    public Graph Kruskal_I(){
        Graph MST = new Graph();
        MST.description = "KruskalI_MST";
        MST.totalWeight = 0.0;
        MST.isDirected = false;
        
        this.vertices.forEach((Integer key, Vertex value) -> {
            MST.AddVertex(key, value);
        });
        
        this.edges.forEach((String key, Edge value) -> {
            MST.AddEdge(key, value);
        });

        ArrayList<Edge> orderedEdges = new ArrayList(this.edges.values());
        Collections.sort(orderedEdges, Collections.reverseOrder());
        
        for (Edge e: orderedEdges) {         
            MST.adjacencyList.get(e.sourceID).remove(e.targetID); 
            MST.adjacencyList.get(e.targetID).remove(e.sourceID); 
            if(!MST.IsConnected()){
                MST.adjacencyList.get(e.sourceID).add(e.targetID);
                MST.adjacencyList.get(e.targetID).add(e.sourceID);                
            } else {
                if(MST.edges.containsKey(e.sourceID+"-"+e.targetID)){
                    MST.edges.remove(e.sourceID+"-"+e.targetID);
                }
                if(MST.edges.containsKey(e.targetID+"-"+e.sourceID)){
                    MST.edges.remove(e.targetID+"-"+e.sourceID);
                }
            }            
        }
        
        MST.edges.forEach((String key, Edge value) -> {
            MST.totalWeight += value.weight;
        });
        
        System.out.println("Peso del MST generado por Kruskal inverso: " + MST.totalWeight);
        
        return MST;
    }
    
    /**
     * Finds Minimum Spanning Tree and calculates total weight
     * @return Minimum Spanning Tree
     */
    public Graph Prim() {
        PriorityQueue<Vertex> pqueue = new PriorityQueue<>();
        Graph mst = new Graph();
        mst.vertices = this.vertices;
        mst.isDirected = false;
        mst.edges = new HashMap<>();
        mst.description = "Prim_MST";
        mst.totalWeight = 0.0;

        this.vertices.forEach((Integer key, Vertex value) -> {
            value.distance = Float.POSITIVE_INFINITY;
            value.explored = false;
            value.parent = null;
            pqueue.add(value);
        });
        
        while(!pqueue.isEmpty()) {
            Vertex u = pqueue.remove();
            u.explored = true;
            
            if(u.parent!=null){
                Edge mstEdge = new Edge();
                mstEdge.sourceID = u.parent.id;
                mstEdge.targetID = u.id;
                mstEdge.weight = GetEdge(mstEdge.sourceID,mstEdge.targetID).weight;
                mst.AddEdge(mstEdge.sourceID+"-"+mstEdge.targetID, mstEdge);
                mst.totalWeight += mstEdge.weight;
            }
                 
            for (Integer value : this.adjacencyList.get(u.id)) {  
                Vertex v = this.vertices.get(value);
                if(!v.explored && (v.distance > GetEdge(u.id,value).weight)){
                    v.distance = GetEdge(u.id,value).weight;
                    v.parent = u;
                    if (pqueue.contains(v)){
                        pqueue.remove(v);
                    }
                    pqueue.add(v);
                }
            }
        }
        
        System.out.println("Peso del MST generado por Prim: " + mst.totalWeight);
        
        return mst;
    }
    
    /**
     * Gets an edge of an undirected graph
     * @param node1
     * @param node2
     * @return the edge
     */
    public Edge GetEdge(Integer node1, Integer node2) {
        String edgeKey = this.edges.containsKey(node1+"-"+node2) ? node1+"-"+node2 : "" ;
        edgeKey = this.edges.containsKey(node2+"-"+node1) ? node2+"-"+node1 : edgeKey;
        
        return this.edges.containsKey(edgeKey) ? this.edges.get(edgeKey) : null;
    }
    
    /**
     * Print a Graph as .Dot language on
     * specific file
     * @param showDistance Idicates whether the distance label must or must not be included
     * @param showVertexWeight Idicates whether the weight label must or must not be included
     * @param ShowTotalWeight Idicates whether the total weight label must or must not be included
     * @throws java.io.FileNotFoundException
     */
    public void PrintToFile(boolean showDistance, boolean showVertexWeight, boolean ShowTotalWeight, String name) throws FileNotFoundException{
                
        PrintWriter writter = new PrintWriter(name+".gv");
        String separator;
        if(this.isDirected){
            writter.print("digraph ");
            separator = " -> ";
        }
        else
        {
            writter.print("graph ");
            separator = " -- ";
        }
        
        writter.print(this.description);
        writter.print("{");
        
        this.vertices.forEach((Integer key, Vertex value) -> {
            String idVertex = value.id.toString();
            String label = " [label = \"Nodo"+idVertex;
            if(showDistance){
                label = label +" ("+value.distance+")";
            }
            if(ShowTotalWeight&&(value.id==1)){               
                label = " [label = \"PesoTotal = "+this.totalWeight;
            }              
            writter.println(idVertex+label+"\"];");
        });
        
        this.edges.forEach((String key, Edge value) -> {
            String source = value.sourceID.toString();
            String target  = value.targetID.toString();
            String label = "";
            if(showVertexWeight){
                label = " [label = \""+value.weight+"\"]";
            }
            writter.println(source+separator+target+label+";");
        });
        
        writter.print("}");
        writter.close();      
    }
}
